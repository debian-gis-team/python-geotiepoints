Source: python-geotiepoints
Maintainer: Debian GIS Project <pkg-grass-devel@lists.alioth.debian.org>
Uploaders: Antonio Valentino <antonio.valentino@tiscali.it>
Section: python
Priority: optional
Rules-Requires-Root: no
Testsuite: autopkgtest-pkg-pybuild
Build-Depends: debhelper-compat (= 13),
               dh-python,
               dh-sequence-numpy3,
               dh-sequence-python3,
               cython3 (>= 3.0),
               pybuild-plugin-pyproject,
               python3-all-dev,
               python3-dask,
               python3-h5py,
               python3-numpy,
               python3-pandas,
               python3-pyproj,
               python3-pyresample,
               python3-pytest <!nocheck>,
               python3-scipy,
               python3-setuptools,
               python3-versioneer,
               python3-xarray
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/debian-gis-team/python-geotiepoints
Vcs-Git: https://salsa.debian.org/debian-gis-team/python-geotiepoints.git
Homepage: https://github.com/pytroll/python-geotiepoints

Package: python3-geotiepoints
Architecture: any
Depends: python3-dask,
         python3-pandas,
         python3-scipy,
         python3-xarray,
         ${python3:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Recommends: python3-h5py
Description: Interpolation of geographic tiepoints in Python
 Python-geotiepoints is a Python module that interpolates (and extrapolates
 if needed) geographical tiepoints into a larger geographical grid.
 This is useful when the full resolution lon/lat grid is needed while only
 a lower resolution grid of tiepoints was provided.
 .
 Some helper functions are provided to accommodate for satellite data, but
 the package should be generic enough to be used for any kind of data.
 .
 In addition python-geotiepoints provides a fast multilinear interpolation
 of regular gridded data using Cython.
